#[macro_use]
extern crate failure;
#[macro_use]
extern crate log;
#[macro_use]
extern crate relm;
#[macro_use]
extern crate relm_derive;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate structopt;
extern crate config;
extern crate directories;
extern crate env_logger;
extern crate gdk_pixbuf;
extern crate gtk;
extern crate pango;
extern crate serde;
extern crate toml;

mod accounts;
mod configuration;
mod main_window;
mod product_list;
mod product_overview;

use configuration::Configuration;
use gtk::{CssProviderExt, IsA, ObjectExt, StyleContextExt, WidgetExt};
use relm::Widget;
use structopt::StructOpt;

type Result<T> = std::result::Result<T, failure::Error>;

#[derive(StructOpt, Debug)]
#[structopt(name = "Samplog", about = "Continuous sample log")]
pub struct Opt {}

fn main() {
    env_logger::init();
    let opt = Opt::from_args();

    match run(opt) {
        Err(s) => {
            warn!("{}", s);
            std::process::exit(2);
        }
        Ok(_) => {
            std::process::exit(0);
        }
    }
}

fn run(opt: Opt) -> Result<()> {
    gtk::init()?;

    let css = include_bytes!("../style/samplog.css");
    let style = gtk::CssProvider::new();
    style.load_from_data(css)?;

    if let Some(settings) = gtk::Settings::get_default() {
        settings
            .set_property("gtk-application-prefer-dark-theme", &false)?;
    }

    let configuration = configuration::Configuration::new()?;

    main_window::W::run((opt, configuration, style))
        .map_err(|_| format_err!("Couldn't run"))?;

    Ok(())
}

trait SetStyle {
    fn set_style(&self, style: &gtk::CssProvider);
}

impl<W: IsA<gtk::Widget> + IsA<gtk::Object> + ObjectExt> SetStyle for W {
    fn set_style(&self, style: &gtk::CssProvider) {
        if let Some(context) = self.get_style_context() {
            context.add_provider(
                style,
                gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
            );
        }
    }
}
