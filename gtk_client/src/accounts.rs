use configuration;
use gtk::{
    BoxExt, ButtonExt, ContainerExt, CssProvider, IconSize, LabelExt,
    ListBoxExt, ListBoxRowExt, OrientableExt, ScrolledWindowExt,
    WidgetExt,
};
use relm::{Relm, Update, Widget};
use std::rc::Rc;
use {gtk, pango, relm, SetStyle};

pub struct Model {
    relm: relm::Relm<W>,
    configuration: Rc<configuration::Configuration>,
    style: CssProvider,
}

#[derive(Msg)]
pub enum Msg {
    RowActivated(Option<i32>),
    AccountActivated(usize),
}

const ACCOUNTS_WIDTH: i32 = 400;

impl Update for W {
    type Model = Model;
    type ModelParam = (Rc<configuration::Configuration>, CssProvider);
    type Msg = Msg;

    fn model(
        relm: &relm::Relm<Self>,
        details: (Rc<configuration::Configuration>, CssProvider),
    ) -> Model {
        let (configuration, style) = details;
        Model {
            relm: relm.clone(),
            configuration,
            style,
        }
    }

    fn update(&mut self, event: Msg) {
        match event {
            Msg::RowActivated(index) => match index {
                Some(index) if index >= 0 => {
                    self.model
                        .relm
                        .stream()
                        .emit(Msg::AccountActivated(index as usize));
                }
                Some(index) => {
                    warn!("Negative index {} selected", index);
                }
                _ => {
                    warn!("No index selected");
                }
            },
            Msg::AccountActivated(_) => {}
        }
    }
}

pub struct W {
    scrolled_window: gtk::ScrolledWindow,
    vbox: gtk::Box,
    account_scroller: gtk::ScrolledWindow,
    list_box: gtk::ListBox,
    model: Model,
}

impl Widget for W {
    type Root = gtk::ScrolledWindow;

    fn root(&self) -> Self::Root {
        self.scrolled_window.clone()
    }

    fn view(relm: &Relm<Self>, model: Self::Model) -> Self {
        let (hbox, vbox, list_box, account_scroller) = {
            let placeholder_label_left = gtk::Label::new(None);
            placeholder_label_left.set_hexpand(true);

            let (vbox, account_scroller, list_box) = {
                let accounts_label = gtk::Label::new("Accounts");
                accounts_label.set_hexpand(true);
                accounts_label.set_property_width_request(ACCOUNTS_WIDTH);
                accounts_label.set_style(&model.style);
                accounts_label.set_name("title");
                accounts_label.set_xalign(0f32);

                let (account_scroller, list_box) = {
                    let list_box = gtk::ListBox::new();
                    connect!(
                        relm,
                        list_box,
                        connect_row_selected(_, row),
                        Msg::RowActivated(
                            row.clone().map(|r| r.get_index())
                        )
                    );

                    let account_scroller =
                        gtk::ScrolledWindow::new(None, None);
                    account_scroller.set_vexpand(true);
                    account_scroller.set_property_hscrollbar_policy(
                        gtk::PolicyType::Never,
                    );
                    account_scroller.set_visible(false);
                    account_scroller.set_style(&model.style);
                    account_scroller.set_name("accounts");

                    account_scroller.add(&list_box);

                    (account_scroller, list_box)
                };

                let button_box = {
                    let add_button = gtk::Button::new();
                    add_button.set_style(&model.style);
                    add_button.set_label(&"Add account".to_string());

                    let button_box =
                        gtk::Box::new(gtk::Orientation::Vertical, 3);

                    button_box.pack_start(&add_button, false, false, 3);

                    button_box
                };

                let vbox = gtk::Box::new(gtk::Orientation::Vertical, 3);
                vbox.set_hexpand(false);
                vbox.set_spacing(3);

                vbox.pack_start(&accounts_label, false, false, 3);
                vbox.pack_start(&account_scroller, true, true, 3);
                vbox.pack_start(&button_box, false, false, 3);

                (vbox, account_scroller, list_box)
            };

            let placeholder_label_right = gtk::Label::new(None);
            placeholder_label_right.set_hexpand(true);

            let hbox = gtk::Box::new(gtk::Orientation::Horizontal, 3);
            hbox.set_style(&model.style);
            hbox.set_name("padded");

            hbox.pack_start(&placeholder_label_left, true, false, 3);
            hbox.pack_start(&vbox, true, true, 3);
            hbox.pack_start(&placeholder_label_right, true, false, 3);

            (hbox, vbox, list_box, account_scroller)
        };

        let scrolled_window = gtk::ScrolledWindow::new(None, None);
        scrolled_window
            .set_property_hscrollbar_policy(gtk::PolicyType::Never);
        scrolled_window.set_vexpand(true);
        scrolled_window.set_hexpand(true);

        scrolled_window.add(&hbox);

        W {
            scrolled_window,
            vbox,
            account_scroller,
            list_box,
            model,
        }
    }

    /*
    view! {
        #[name="scrolled_window"]
        gtk::ScrolledWindow {
            vexpand: true,
            hexpand: true,
            property_hscrollbar_policy: gtk::PolicyType::Never,
            gtk::Box {
                orientation: gtk::Orientation::Horizontal,
                style: &self.model.style,
                name: "padded",
                gtk::Label{
                    hexpand: true,
                },
                #[name="vbox"]
                gtk::Box {
                    orientation: gtk::Orientation::Vertical,
                    hexpand: false,
                    spacing: 5,
                    gtk::Label{
                        hexpand: true,
                        property_width_request: ACCOUNTS_WIDTH,
                        text: &"Accounts".to_string(),
                        style: &self.model.style,
                        name: "title",
                        xalign: 0f32,
                    },
                    #[name="account_scroller"]
                    gtk::ScrolledWindow {
                        vexpand: true,
                        property_hscrollbar_policy: gtk::PolicyType::Never,
                        property_width_request: ACCOUNTS_WIDTH,
                        visible: false,
                        style: &self.model.style,
                        name: "accounts",
                        #[name="list_box"]
                        gtk::ListBox {
                            row_selected(_, row) =>
                                Msg::RowActivated(row.clone().map(|r|r.get_index()))
                        },
                    },
                    gtk::Box {
                        orientation: gtk::Orientation::Vertical,
                        gtk::Button {
                            style: &self.model.style,
                            label: &"Add account".to_string(),
                        }
                    }
                },
                gtk::Label{
                    hexpand: true,
                },
            }
        }
    }
    */

    fn init_view(&mut self) {
        let accounts = &self.model.configuration.accounts;
        let has_accounts = accounts.len() > 0;

        let placeholder = gtk::Label::new("No accounts configured");
        placeholder.set_style(&self.model.style);
        placeholder.set_yalign(0f32);
        placeholder.set_style(&self.model.style);
        placeholder.set_name("placeholder");
        placeholder.show();
        self.list_box.set_placeholder(&placeholder);

        for ref account in &self.model.configuration.accounts {
            let hbox = gtk::Box::new(gtk::Orientation::Horizontal, 5);

            let labelbox = {
                let name = gtk::Label::new(account.name.as_str());
                name.set_style(&self.model.style);
                name.set_name("name");
                name.set_xalign(0f32);
                name.set_ellipsize(pango::EllipsizeMode::End);

                let url = gtk::Label::new(account.url.as_str());
                url.set_style(&self.model.style);
                url.set_name("url");
                url.set_xalign(0f32);
                url.set_ellipsize(pango::EllipsizeMode::End);

                let b = gtk::Box::new(gtk::Orientation::Vertical, 5);
                b.pack_end(&url, true, true, 4);
                b.pack_start(&name, true, true, 4);
                b
            };

            let buttonbox = {
                let button = gtk::Button::new_from_icon_name(
                    "gtk-edit",
                    IconSize::Button.into(),
                );

                let b = gtk::Box::new(gtk::Orientation::Vertical, 5);
                b.pack_start(&button, true, false, 4);
                b
            };

            hbox.pack_start(&labelbox, true, true, 4);
            hbox.pack_end(&buttonbox, false, false, 4);

            hbox.set_name("listrow");
            hbox.set_style(&self.model.style);

            self.list_box.add(&hbox);
        }
    }
}
