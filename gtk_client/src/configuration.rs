use config;
use directories;
use std::fs::{create_dir_all, File};
use std::io::Write;
use std::path::{Path, PathBuf};
use toml;

use Result;

#[derive(Clone, Default, Debug, Deserialize, Serialize)]
pub struct Account {
    pub name: String,
    pub url: String,
}

#[derive(Default, Debug, Deserialize, Serialize)]
pub struct Configuration {
    #[serde(rename = "account")]
    pub accounts: Vec<Account>,
}

impl Configuration {
    fn path() -> PathBuf {
        let project_dirs =
            directories::ProjectDirs::from("org", "Samplog", "Samplog");
        project_dirs.config_dir().join("config.toml")
    }

    pub fn new() -> Result<Self> {
        let path = Self::path();

        info!("Using config file: {:?}", Self::path());
        if !Path::new(&path).exists() {
            info!("Config file not present, creating with default values");
            Self::default().save()?;
        }

        let mut loaded = config::Config::default();
        loaded.merge(
            config::File::with_name(Self::path().to_str().unwrap())
                .required(true),
        )?;
        Ok(loaded.try_into()?)
    }

    pub fn save(&self) -> Result<()> {
        let toml_string = toml::to_string(&self)?;
        let path = Self::path();
        create_dir_all(&path.parent().unwrap())?;
        File::create(&path)?.write_all(toml_string.as_bytes())?;
        Ok(())
    }
}
