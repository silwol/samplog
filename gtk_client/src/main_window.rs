use configuration;
use gtk::{
    ButtonExt, ContainerExt, CssProvider, GtkWindowExt, HeaderBarExt,
    StackExt, WidgetExt,
};
use relm::{ContainerWidget, Relm, Update, Widget};
use std;
use std::rc::Rc;
use {
    accounts, gtk, product_list, product_overview, relm, Configuration,
    Opt,
};

enum ChildPage {
    Nothing,
    Accounts(relm::Component<accounts::W>),
    ProductList(relm::Component<product_list::W>),
    ProductPage(relm::Component<product_overview::W>),
}

pub struct Model {
    relm: relm::Relm<W>,
    child_page: ChildPage,
    options: Opt,
    configuration: Rc<Configuration>,
    connection: Option<(String, configuration::Account)>,
    style: CssProvider,
}

#[derive(Msg)]
pub enum Msg {
    Quit,
    AccountSelected(usize),
    ProductSelected,
    GoBack,
}

impl Update for W {
    type Model = Model;
    type ModelParam = (Opt, Configuration, CssProvider);
    type Msg = Msg;

    fn model(
        relm: &relm::Relm<Self>,
        details: (Opt, Configuration, CssProvider),
    ) -> Model {
        let (options, configuration, style) = details;
        Model {
            relm: relm.clone(),
            child_page: ChildPage::Nothing,
            options,
            configuration: Rc::new(configuration),
            connection: None,
            style,
        }
    }

    fn update(&mut self, event: Msg) {
        match event {
            Msg::Quit => gtk::main_quit(),
            Msg::AccountSelected(index) => {
                let account = &self.model.configuration.accounts[index];
                // TODO
                println!(
                    "Account {:?} selected: {:?}",
                    account.name, account.url
                );
            }
            Msg::ProductSelected => {
                println!("Product selected");
                self.show_product(gtk::StackTransitionType::SlideLeft);
            }
            Msg::GoBack => {
                println!("go back");
                self.show_product_list(
                    gtk::StackTransitionType::SlideRight,
                );
            }
        }
    }
}

pub struct W {
    window: gtk::Window,
    stack: gtk::Stack,
    back_button: gtk::Button,
    model: Model,
}

impl Widget for W {
    type Root = gtk::Window;

    fn root(&self) -> Self::Root {
        self.window.clone()
    }

    fn view(relm: &Relm<Self>, model: Self::Model) -> Self {
        let (header_bar, back_button) = {
            let back_button = {
                let back_button = gtk::Button::new();
                back_button.set_visible(false);
                back_button.set_image(&gtk::Image::new_from_icon_name(
                    "go-previous",
                    gtk::IconSize::LargeToolbar.into(),
                ));
                connect!(
                    relm,
                    back_button,
                    connect_clicked(_),
                    Msg::GoBack
                );
                back_button
            };

            let header_bar = gtk::HeaderBar::new();
            header_bar.set_title(Some("Samplog"));
            header_bar.set_show_close_button(true);
            header_bar.pack_start(&back_button);

            (header_bar, back_button)
        };

        let stack = gtk::Stack::new();

        let window = gtk::Window::new(gtk::WindowType::Toplevel);
        window.set_titlebar(&header_bar);
        window.add(&stack);

        connect!(
            relm,
            window,
            connect_delete_event(_, _),
            return (Some(Msg::Quit), gtk::Inhibit(false))
        );

        window.show_all();

        W {
            window,
            stack,
            back_button,
            model,
        }
    }

    fn init_view(&mut self) {
        use gtk::{ContainerExt, GtkWindowExt};

        self.stack.set_size_request(300, 300);
        self.show_accounts(gtk::StackTransitionType::None);
    }

    /*
    view! {
        #[name="window"]
        gtk::Window {
            #[name="vbox"]
            gtk::Box {
                orientation: gtk::Orientation::Vertical,
                #[name="stack"]
                gtk::Stack {},
                #[name="titlebar"]
                gtk::HeaderBar {
                    title: Some("Samplog"),
                    show_close_button: true,
                    #[name="back_button"]
                    gtk::Button {
                        visible: false,
                        image: &gtk::Image::new_from_icon_name(
                                   "go-previous", gtk::IconSize::LargeToolbar.into(),),
                        clicked(_) => Msg::GoBack,
                    }
                },
            },
            delete_event(_,_) => (Msg::Quit,gtk::Inhibit(false)),
        },
    }
    */
}

impl W {
    fn show_accounts(&mut self, transition: gtk::StackTransitionType) {
        use accounts::Msg::AccountActivated;
        use main_window::Msg::AccountSelected;
        use relm::ContainerWidget;

        let widget = self.stack.add_widget::<accounts::W>((
            Rc::clone(&self.model.configuration),
            self.model.style.clone(),
        ));
        widget.widget().show_all();
        self.stack
            .set_child_name(widget.widget(), "accounts");
        self.stack
            .set_visible_child_full("accounts", transition);

        self.back_button.set_visible(false);

        connect!(
            widget@AccountActivated(index),
            &self.model.relm,
            AccountSelected(index));

        let mut c: ChildPage = ChildPage::Accounts(widget);
        std::mem::swap(&mut c, &mut self.model.child_page);
        self.remove_previous_page(c);
    }

    fn show_product_list(&mut self, transition: gtk::StackTransitionType) {
        use main_window::Msg::ProductSelected;
        use product_list::Msg::RowActivated;
        use relm::ContainerWidget;

        let widget = self.stack.add_widget::<product_list::W>(());
        widget.widget().show_all();
        self.stack
            .set_child_name(widget.widget(), "product_list");
        self.stack
            .set_visible_child_full("product_list", transition);

        self.back_button.set_visible(false);

        connect!(widget@RowActivated, &self.model.relm, ProductSelected);

        let mut c: ChildPage = ChildPage::ProductList(widget);
        std::mem::swap(&mut c, &mut self.model.child_page);
        self.remove_previous_page(c);
    }

    fn show_product(&mut self, transition: gtk::StackTransitionType) {
        use relm::ContainerWidget;

        let widget = self.stack.add_widget::<product_overview::W>(());
        widget.widget().show_all();
        self.stack
            .set_child_name(widget.widget(), "product_overview");
        self.stack
            .set_visible_child_full("product_overview", transition);

        self.back_button.set_visible(true);

        let mut c: ChildPage = ChildPage::ProductPage(widget);
        std::mem::swap(&mut c, &mut self.model.child_page);
        self.remove_previous_page(c);
    }

    fn remove_previous_page(&mut self, p: ChildPage) {
        match p {
            ChildPage::Nothing => {}
            ChildPage::Accounts(child) => {
                self.stack.remove_widget(child);
            }
            ChildPage::ProductList(child) => {
                self.stack.remove_widget(child);
            }
            ChildPage::ProductPage(child) => {
                self.stack.remove_widget(child);
            }
        }
    }
}
