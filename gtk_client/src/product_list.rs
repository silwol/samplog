use gtk::{
    BoxExt, CellLayoutExt, ContainerExt, LabelExt, ListStoreExtManual,
    OrientableExt, TreeViewColumnExt, TreeViewExt, WidgetExt,
};
use relm::{Relm, Update, Widget};
use {gtk, relm};

pub struct Model {
    products: gtk::ListStore,
}

#[derive(Msg)]
pub enum Msg {
    RowActivated,
}

impl Update for W {
    type Model = Model;
    type ModelParam = ();
    type Msg = Msg;

    fn model(_: &relm::Relm<Self>, _: ()) -> Model {
        let products =
            gtk::ListStore::new(&[gtk::Type::String, gtk::Type::String]);
        products.insert_with_values(
            None,
            &[0, 1],
            &[&"X1000", &"Very fine paper"],
        );
        products.insert_with_values(
            None,
            &[0, 1],
            &[&"F321", &"Medium paper"],
        );
        products.insert_with_values(
            None,
            &[0, 1],
            &[&"N2X1002", &"Very thick and stiff paper"],
        );
        Model { products }
    }

    fn update(&mut self, event: Msg) {
        match event {
            Msg::RowActivated => {}
        }
    }
}

pub struct W {
    vbox: gtk::Box,
    scrolled_window: gtk::ScrolledWindow,
    search_entry: gtk::Entry,
    tree_view: gtk::TreeView,
    model: Model,
}

impl Widget for W {
    type Root = gtk::Box;

    fn root(&self) -> Self::Root {
        self.vbox.clone()
    }

    fn view(relm: &Relm<Self>, model: Self::Model) -> Self {
        let (vbox, search_entry, scrolled_window, tree_view) = {
            let (hbox, search_entry) = {
                let placeholder_label = gtk::Label::new(None);
                placeholder_label.set_hexpand(true);

                let search_label = gtk::Label::new("Search");

                let search_entry = gtk::Entry::new();

                let hbox = gtk::Box::new(gtk::Orientation::Horizontal, 3);
                hbox.set_spacing(3);
                hbox.set_border_width(3);

                hbox.pack_start(&placeholder_label, true, false, 3);
                hbox.pack_start(&search_label, false, false, 3);
                hbox.pack_start(&search_entry, false, false, 3);

                (hbox, search_entry)
            };

            let (scrolled_window, tree_view) = {
                let tree_view = gtk::TreeView::new();
                connect!(
                    relm,
                    tree_view,
                    connect_row_activated(_, _, _),
                    Msg::RowActivated
                );

                let scrolled_window = gtk::ScrolledWindow::new(None, None);
                scrolled_window.set_vexpand(true);
                scrolled_window.set_hexpand(true);

                scrolled_window.add(&tree_view);

                (scrolled_window, tree_view)
            };

            let vbox = gtk::Box::new(gtk::Orientation::Vertical, 3);

            vbox.pack_start(&hbox, false, false, 3);
            vbox.pack_start(&scrolled_window, true, true, 3);

            (vbox, search_entry, scrolled_window, tree_view)
        };

        W {
            vbox,
            tree_view,
            search_entry,
            scrolled_window,
            model,
        }
    }

    /*
    view! {
        #[name="vbox"]
        gtk::Box {
            orientation: gtk::Orientation::Vertical,
            gtk::Box {
                orientation:gtk::Orientation::Horizontal,
                spacing: 3,
                border_width: 3,
                gtk::Label {
                    hexpand: true,
                },
                gtk::Label {
                    text: &"Search:".to_string(),
                },
                #[name="search"]
                gtk::Entry {
                },
            },
            #[name="scrolled_window"]
            gtk::ScrolledWindow {
                vexpand: true,
                hexpand: true,
                #[name="tree_view"]
                gtk::TreeView{
                    row_activated(_,_,_) => Msg::RowActivated,
                },
            },
        },
    }
    */

    fn init_view(&mut self) {
        self.tree_view.set_model(&self.model.products);

        {
            // name col
            let col = gtk::TreeViewColumn::new();
            let renderer = gtk::CellRendererText::new();
            col.set_title("Name");
            col.pack_start(&renderer, false);
            col.add_attribute(&renderer, "text", 0);
            self.tree_view.append_column(&col);
        }

        {
            // description col
            let col = gtk::TreeViewColumn::new();
            let renderer = gtk::CellRendererText::new();
            col.set_title("Description");
            col.pack_start(&renderer, false);
            col.add_attribute(&renderer, "text", 1);
            self.tree_view.append_column(&col);
        }

        self.tree_view
            .set_search_entry(&self.search_entry);
        self.tree_view.show_all();
    }
}
