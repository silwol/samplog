use super::{gtk, relm};
use gdk_pixbuf::Pixbuf;
use gtk::{
    BoxExt, ButtonExt, CellLayoutExt, ContainerExt, ImageExt, LabelExt,
    ListStoreExtManual, OrientableExt, TreeViewColumnExt, TreeViewExt,
    WidgetExt,
};
use relm::{Relm, Update, Widget};

pub struct Model {
    id: String,
    description: String,
    samples: gtk::ListStore,
    trend_image: Pixbuf,
}

#[derive(Msg)]
pub enum Msg {}

impl Update for W {
    type Model = Model;
    type ModelParam = ();
    type Msg = Msg;

    fn model(_: &relm::Relm<Self>, _: ()) -> Model {
        let id = "X1000".to_string();
        let description = "Very fine paper".to_string();
        let samples = gtk::ListStore::new(&[
            gtk::Type::String,
            gtk::Type::String,
            gtk::Type::String,
            gtk::Type::String,
        ]);
        samples.insert_with_values(
            None,
            &[0, 1, 2, 3],
            &[&"2018-03-12 08:17", &"6864235", &"254μm", &"-"],
        );
        samples.insert_with_values(
            None,
            &[0, 1, 2, 3],
            &[&"2018-01-08 13:30", &"6864185", &"250μm", &"-"],
        );
        samples.insert_with_values(
            None,
            &[0, 1, 2, 3],
            &[
                &"2017-12-10 10:06",
                &"6864153",
                &"239μm",
                &"137g",
            ],
        );
        samples.insert_with_values(
            None,
            &[0, 1, 2, 3],
            &[
                &"2017-09-12 08:51",
                &"6864079",
                &"254μm",
                &"132g",
            ],
        );
        samples.insert_with_values(
            None,
            &[0, 1, 2, 3],
            &[
                &"2017-04-14 08:13",
                &"6863216",
                &"254μm",
                &"132g",
            ],
        );
        samples.insert_with_values(
            None,
            &[0, 1, 2, 3],
            &[
                &"2017-03-01 15:00",
                &"6863131",
                &"254μm",
                &"132g",
            ],
        );
        samples.insert_with_values(
            None,
            &[0, 1, 2, 3],
            &[
                &"2017-01-10 18:53",
                &"6861900",
                &"254μm",
                &"132g",
            ],
        );
        samples.insert_with_values(
            None,
            &[0, 1, 2, 3],
            &[
                &"2016-08-27 13:37",
                &"6861856",
                &"254μm",
                &"132g",
            ],
        );
        samples.insert_with_values(
            None,
            &[0, 1, 2, 3],
            &[
                &"2016-03-02 09:03",
                &"6861812",
                &"254μm",
                &"132g",
            ],
        );
        samples.insert_with_values(
            None,
            &[0, 1, 2, 3],
            &[
                &"2016-01-12 11:17",
                &"6860959",
                &"254μm",
                &"132g",
            ],
        );

        let trend_image = Pixbuf::new_from_file_at_size(
            "mockup_images/trend_chart.svg",
            100,
            75,
        ).unwrap();

        Model {
            id,
            description,
            samples,
            trend_image,
        }
    }

    fn update(&mut self, event: Msg) {
        match event {}
    }
}

pub struct W {
    vbox: gtk::Box,
    tree_view: gtk::TreeView,
    model: Model,
}

impl Widget for W {
    type Root = gtk::Box;

    fn root(&self) -> Self::Root {
        self.vbox.clone()
    }

    fn view(relm: &Relm<Self>, model: Self::Model) -> Self {
        let top_hbox = {
            let (label_model_id, label_placeholder, button_new) = {
                let label_model_id = gtk::Label::new(None);
                label_model_id.set_text(&model.id.to_string());

                let label_placeholder = gtk::Label::new(None);
                label_placeholder.set_hexpand(true);

                let button_new = gtk::Button::new();
                button_new.set_label(&"Add sample".to_string());

                (label_model_id, label_placeholder, button_new)
            };

            let top_hbox = gtk::Box::new(gtk::Orientation::Horizontal, 3);
            top_hbox.set_spacing(3);
            top_hbox.set_border_width(3);
            top_hbox.pack_start(&label_model_id, true, false, 3);
            top_hbox.pack_start(&label_placeholder, true, true, 3);
            top_hbox.pack_start(&button_new, true, false, 3);
            top_hbox
        };
        let flow_box = {
            let thickness_vbox = {
                let thickness_label = gtk::Label::new("Thickness");

                let thickness_image = gtk::Image::new();
                thickness_image.set_from_pixbuf(&model.trend_image);

                let thickness_vbox =
                    gtk::Box::new(gtk::Orientation::Horizontal, 3);
                thickness_vbox.pack_start(
                    &thickness_label,
                    false,
                    false,
                    3,
                );
                thickness_vbox.pack_start(
                    &thickness_image,
                    false,
                    false,
                    3,
                );
                thickness_vbox
            };
            let weight_vbox = {
                let weight_label = gtk::Label::new("Weight");

                let weight_image = gtk::Image::new();
                weight_image.set_from_pixbuf(&model.trend_image);

                let weight_vbox =
                    gtk::Box::new(gtk::Orientation::Horizontal, 3);
                weight_vbox.pack_start(&weight_label, false, false, 3);
                weight_vbox.pack_start(&weight_image, false, false, 3);
                weight_vbox
            };

            let flow_box = gtk::FlowBox::new();
            flow_box.add(&thickness_vbox);
            flow_box.add(&weight_vbox);
            flow_box
        };

        let (scrolled_window, tree_view) = {
            let tree_view = gtk::TreeView::new();

            let scrolled_window = gtk::ScrolledWindow::new(None, None);
            scrolled_window.set_vexpand(true);
            scrolled_window.set_hexpand(true);
            scrolled_window.add(&tree_view);

            (scrolled_window, tree_view)
        };

        let vbox = gtk::Box::new(gtk::Orientation::Vertical, 3);

        vbox.pack_start(&top_hbox, false, false, 3);
        vbox.pack_start(&flow_box, false, false, 3);
        vbox.pack_start(&scrolled_window, true, true, 3);

        W {
            vbox,
            tree_view,
            model,
        }
    }

    /*
    view! {
        #[name="vbox"]
        gtk::Box {
            orientation: gtk::Orientation::Vertical,
            gtk::Box {
                orientation:gtk::Orientation::Horizontal,
                spacing: 3,
                border_width: 3,
                gtk::Label {
                    text: &self.model.id.to_string(),
                },
                gtk::Label {
                    hexpand: true,
                },
                #[name="new"]
                gtk::Button {
                    label: &"Add sample".to_string(),
                },
            },

            gtk::FlowBox {
                gtk::Box {
                    orientation: gtk::Orientation::Vertical,
                    gtk::Label {
                        text: &"Thickness".to_string(),
                    },
                    gtk::Image {
                        from_pixbuf: &self.model.trend_image,
                    },
                },
                gtk::Box {
                    orientation: gtk::Orientation::Vertical,
                    gtk::Label {
                        text: &"Weight".to_string(),
                    },
                    gtk::Image {
                        from_pixbuf: &self.model.trend_image,
                    },
                },
            },

            #[name="scrolled_window"]
            gtk::ScrolledWindow {
                vexpand: true,
                hexpand: true,
                #[name="tree_view"]
                gtk::TreeView{
                    //row_activated(_,_,_) => Msg::RowActivated,
                },
            },
        },
    }
    */

    fn init_view(&mut self) {
        self.tree_view.set_model(&self.model.samples);

        {
            // date
            let col = gtk::TreeViewColumn::new();
            let renderer = gtk::CellRendererText::new();
            col.set_title("Date");
            col.pack_start(&renderer, false);
            col.add_attribute(&renderer, "text", 0);
            self.tree_view.append_column(&col);
        }

        {
            // sample number
            let col = gtk::TreeViewColumn::new();
            let renderer = gtk::CellRendererText::new();
            col.set_title("Sample No");
            col.pack_start(&renderer, false);
            col.add_attribute(&renderer, "text", 1);
            self.tree_view.append_column(&col);
        }

        {
            // thickness
            let col = gtk::TreeViewColumn::new();
            let renderer = gtk::CellRendererText::new();
            col.set_title("Thickness");
            col.pack_start(&renderer, false);
            col.add_attribute(&renderer, "text", 2);
            self.tree_view.append_column(&col);
        }

        {
            // weight
            let col = gtk::TreeViewColumn::new();
            let renderer = gtk::CellRendererText::new();
            col.set_title("Weight");
            col.pack_start(&renderer, false);
            col.add_attribute(&renderer, "text", 3);
            self.tree_view.append_column(&col);
        }

        self.tree_view.show_all();
    }
}
